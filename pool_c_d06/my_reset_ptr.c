#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void my_reset_ptr(char **ptr)
{
  free(*ptr);
  *ptr = NULL;
}

int main(int argc, char const *argv[])
{
  char toto[] = "Salut je fonctionne !";
  int len = strlen(toto);
  char *titi = "Ou pas :)";
  int len_titi = strlen(titi);
  char *dest = malloc(sizeof(*toto) * (len + len_titi + 1));

  my_reset_ptr(&dest);
  if (dest != NULL)
    printf("Ptr is not null :(\n");
  else
    printf("Ok\n");
free(dest);
    return 0;
}
