#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

void	my_show_str(char **str)
{
  int	i;
  int	j;

  i = -1;
  while (str[++i])
    {
      j = -1;
      while (str[i][++j])
	{
	  write(1, &(str[i][j]), 1);
	}
      write(1, "\n", 1);
    }
}

int main(int argc, char const *argv[])
{
	char *tab[] = {
		"Hello",
		"Students",
		"ZogZog",
		"BOOUUUUM",
		"",
		NULL
	};

	my_show_str(tab);
	(void)argc;
	(void)argv;
	return 0;
}
