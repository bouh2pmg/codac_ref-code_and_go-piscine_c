#include <stdlib.h>
#include <stdio.h>
#include <string.h>

char	*my_strdup(char *str)
{
  char	*out;
  int	i;

  i = -1;
  while (str[++i]);

  if ((out = malloc(sizeof(*out) * (i + 1))) == NULL)
    return(NULL);

  i = -1;
  while (str[++i])
    out[i] = str[i];

  out[i] = 0;
  return(out);
}

int main(int argc, char const *argv[])
{
	char *str1 = "coucou";
	char *str2 = "";
	char *str3 = "Il était une fois....";
	char *ret1;
	char *ret2;
	char *ret3;

	ret1 = my_strdup(str1);
	ret2 = my_strdup(str2);
	ret3 = my_strdup(str3);
	printf("%s\n", ret1);
	printf("%s\n", ret2);
	printf("%s\n", ret3);
	ret1[2] = 'z';
	ret2[0] = 'a';
	ret3[6] = 'b';
	printf("%s\n", ret1);
	printf("%c\n", ret2[0]);
	printf("%s\n", ret3);
	int len = strlen(ret1);
	if (ret1[len] != '\0')
	  printf("Fail Last Character\n");
	free(ret1);
	free(ret2);
	free(ret3);
	(void)argc;
	(void)argv;
	return 0;
}
