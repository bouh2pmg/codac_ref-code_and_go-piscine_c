#include <string.h>
#include <stdio.h>

char *my_revstr(char *str)
{
  int i = -1;
  char tmp;
  int len = strlen(str);
  int max = len / 2 - 1;

  while (++i <= max)
    {
      tmp = str[i];
      str[i] = str[len - i - 1];
      str[len - i - 1] = tmp;
    }
  return str;
}

int main()
{
  char sigma[6] = "sigma";
  char alpha[6] = "alpha";
  char beta[5] = "beta";

  printf("%s\n", my_revstr(sigma));
  printf("%s\n", my_revstr(alpha));
  printf("%s\n", my_revstr(beta));
	return 0;
}
