#include <stdio.h>

char *my_strupcase(char *str)
{
  int i = 0;

  while (str && str[i])
    {
      if (str[i] >= 'a' && str[i] <= 'z')
	     str[i] = str[i] - 'a' + 'A';
      ++i;
    }
  return str;
}

int main()
{
	char str1[] = "Taritifico";
	char str2[] = "Apocalypticodramatique";
	char str3[] = "AAAAAAAAAAAAAAAAAAAAAAAAA";
	char str4[] = "zZzZzZzZzZzZ";
	char str5[] = "";

	printf("%s\n", my_strupcase(str1));
	printf("%s\n", my_strupcase(str2));
	printf("%s\n", my_strupcase(str3));
	printf("%s\n", my_strupcase(str4));
	printf("%s\n", my_strupcase(str5));
	return 0;
}
