#include <stdio.h>

int my_str_isalpha(char *str)
{
  int i = 0;

  if (!str || !str[i])
    return 0;
  while (str && str[i])
    {
      if (!((str[i] >= 'a' && str[i] <= 'z')
	    || (str[i] >= 'A' && str[i] <= 'Z')))
	return 0;
      ++i;
    }
  return 1;
}

int main()
{
	char str1[] = "Taritifico";
	char str2[] = "Apocalyp_-_ticodramatique";
	char str3[] = "AAAAAAAAA666AAAAAAAAAAAAAAAA";
	char str4[] = "zZzZzZzZzZzZ()";
	char str5[] = "";

	printf("%d\n", my_str_isalpha(str1));
	printf("%d\n", my_str_isalpha(str2));
	printf("%d\n", my_str_isalpha(str3));
	printf("%d\n", my_str_isalpha(str4));
	printf("%d\n", my_str_isalpha(str5));
	return 0;
}
