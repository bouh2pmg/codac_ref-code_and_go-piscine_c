#include <stdio.h>

int   my_strcmp(char *s1, char *s2)
{
  int i;

  i = 0;
  while (s1[i] != '\0' && s2[i] != '\0' && s2[i] == s1[i])
    i++;
  return (s1[i] - s2[i]);
}

int main()
{
	char *str1 = "lol";
	char *str2 = "Coucou !";
	char *str3 = "Hmmm.. ok ! see you tomorrow";
	char str4[4] = "Tot";

	printf("expected:\t%d\t-\t%d\n", 41, my_strcmp(str1, str2));
	printf("expected:\t%d\t-\t%d\n", 0, my_strcmp(str1, str1));
	printf("expected:\t%d\t-\t%d\n", -24, my_strcmp(str4, str1));
	printf("expected:\t%d\t-\t%d\n", -12, my_strcmp(str3, str4));
	printf("expected:\t%d\t-\t%d\n", 12, my_strcmp(str4, str3));
	return 0;
}
