#include <stdio.h>

int array_sum(int *tab, int size)
{
  int i = -1;
  int sum;

  sum = 0;
  while (++i < size)
    sum += tab[i];
  return sum;
}

int main(int argc, char const *argv[])
{
  int tab[4] = {3,4,5,6};
  int fake[2] = {1,2};
  int tab2[5] = {1,2,3,4,5};

  printf("%d\n", array_sum(tab, 4));
  printf("%d\n", array_sum(fake, -1));
  printf("%d\n", array_sum(tab2, 4)); // Yes it's 4 on purpose
  return 0;
}
