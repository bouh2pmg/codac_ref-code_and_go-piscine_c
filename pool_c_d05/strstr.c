#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *my_strstr(char *s1, char *s2)
{
  int i;
  int j;
  int tmp;

  i = -1;
  while (s1[++i])
  {
    j = 0;
    tmp = i;
    while (s1[i] && s2[j] && s1[i] == s2[j])
    {
      i++;
      j++;
    }
    if (!s2[j])
      return (&s1[tmp]);
    i = tmp;
  }
  return (NULL);
}

int main()
{
  char tab[7] = "abcdef";
  char total[27] = "abcdefghijklmnopqrstuvwxyz";
  char search[2] = "g";
  char gg[7] = "aGgaga";

  printf("expected: %s - %s\n", strstr(total, tab), my_strstr(total, tab));
  printf("expected: %s - %s\n", strstr(gg, search), my_strstr(gg, search));
  if (my_strstr(search, gg) == NULL)
    printf("last test: OK\n");
  else
    printf("last test: KO\n");
  return 0;
}
