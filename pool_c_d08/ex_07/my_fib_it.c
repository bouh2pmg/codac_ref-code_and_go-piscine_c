#include <stdio.h>

int	fib_it(int n)
{
  int	tmp;
  int	f1;
  int	f2;

  if (n < 0)
    return (-1);
  f1 = 0;
  f2 = 1;
  while (0 < n)
    {
      tmp = f1;
      f1 = f2;
      f2 = f1 + tmp;
      n--;
    }
  return (f1);
}

int	main()
{
  int	res1;
  int	res2;
  int	res3;
  int	res4;

  res1 = fib_it(10);
  res2 = fib_it(0);
  res3 = fib_it(-4);
  res4 = fib_it(42);

  if (res1 != 10)
    printf("%d : Hey what you say !?\n", res1);
  printf("%d : NULL\n", res2);
  printf("%d : BibadiBabida Bouuuu !!!!\n", res3);
  printf("%d\n", res4);
}
