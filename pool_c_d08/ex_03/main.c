#include <stdlib.h>
#include <stdio.h>
#include "abs.h"
#include "abs.h"

int main(int argc, char const *argv[])
{
	printf("%d\n", MY_ABS(21));
	printf("%d\n", ((MY_ABS(-42) + 12) * 2) * 14);
	printf("%d\n", MY_ABS(0));
	printf("%d\n", MY_ABS(-98));
	printf("%d\n", MY_ABS(21) * MY_ABS(2));
	(void)argc;
	(void)argv;
	return 0;
}
