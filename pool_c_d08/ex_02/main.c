#include <stdlib.h>
#include <stdio.h>
#include "struct.h"
#include "struct.h"

void my_init(t_my_struct *);

int main(int argc, char const *argv[])
{
	struct s_my_struct s;

	s.id = 42;
	s.str = "lol";
	my_init(&s);
	if (s.id == 0)
	{
		printf("Integer initialisation completed\n");
	}
	else
	{
		printf("Integer initialisation failed\n");
	}
	if (s.str == NULL)
	{
		printf("String initialisation completed\n");
	}
	else
	{
		printf("String initialisation failed\n");
	}
	//	my_init(NULL);
	(void)argc;
	(void)argv;
	return 0;
}
