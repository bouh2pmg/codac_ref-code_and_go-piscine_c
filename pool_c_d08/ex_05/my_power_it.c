#include <stdlib.h>
#include <stdio.h>

int	my_power_it(int nb, int power)
{
  int	ret;

  ret = 1;
  while (power-- > 0)
    ret *= nb;
  return ret;
}

int main(int argc, char const *argv[])
{
	printf("%d\n", my_power_it(2, 4));
	printf("%d\n", my_power_it(26, 6));
	printf("%d\n", my_power_it(11, 1));
	printf("%d\n", my_power_it(11, 0));
	(void)argc;
	(void)argv;
	return 0;
}
