#include <stdlib.h>
#include <stdio.h>
#include "struct.h"
#include "struct.h" // pour tester que vous ayez bien protégé votre .h (ifndef)

int main(int argc, char const *argv[])
{
	struct s_my_struct s;

	s.id = 42;
	s.str = "Codac !";
	printf("%d\n", s.id);
	printf("%s\n", s.str);
	(void)argc;
	(void)argv;
	return 0;
}
