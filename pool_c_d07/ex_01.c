#include <stdio.h>

int	main(int ac, char **av)
{
  int	i;
  i = 0;
  if (ac == 1)
    printf("\n");
  while (++i < ac)
    printf("%s\n", av[i]);
  return (0);
}
