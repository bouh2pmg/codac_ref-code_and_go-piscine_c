#include <unistd.h>

// Version 1 "Classique"
void my_putchar(char c)
{
  write(1, &c, 1);
}

void  my_putstr_classic(char *str)
{
  int i;

  i = -1;
  while (str[++i])
    my_putchar(str[i]);
}

// --------------------------------------------------------------------------
// Version 2 "Avancée" plus optimisée (moins d'appels système -> write)
int   my_strlen(char *str)
{
  int i = -1;
  while (str[++i]);
  return (i);
}

void  my_putstr(char *str)
{
  write(1, str, my_strlen(str));
}

//---------------------------------------------------------------------------
// Main de test
int	main()
{
  my_putstr("Hey my friend\n");
  my_putstr("Do they build a lot ?\n");
  my_putstr("What is your Name ? What is you favourite color in your hair and in the sky, this is a very long test I try to do. You must bring us a Shruberry. Weeee areee the knight who sayyyy Ni ni ni ni ni !! Now, we are the knights who sayyy EikyEikyEikyEikyPatong ....\n");
  return (0);
}
