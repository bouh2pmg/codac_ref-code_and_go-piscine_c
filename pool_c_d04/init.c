#include <stdio.h>

void	my_init(int *i)
{
  *i = 42;
}

int	main()
{
  int	i;

  i = 0;
  printf("before init : %d\n", i);
  my_init(&i);
  printf("after init : %d\n", i);
}
