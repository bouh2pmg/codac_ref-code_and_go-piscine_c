#include <stdio.h>
#include <stdlib.h>

int	*merge_array(int *arr1, int *arr2, int size1, int size2)
{
  int	i;
  int	*ret;

  if (!(ret = malloc(sizeof(*ret) * (size1 + size2))))
    return (NULL);
  i = 0;
  while (i < size1)
    {
      ret[i] = arr1[i];
      ++i;
    }
  i = 0;
  while (i < size2)
    {
      ret[i + size1] = arr2[i];
      ++i;
    }
  return (ret);
}

int	main()
{
  int tab[] = {0};
  int tab2[] = {0, 1, 2, 0};
  int tab3[] = {0, 1, 2, 3, 0, 74, 0, 0, 2, 2};
  int tab4[] = {2, 3};

  int size = 1;
  int size2 = 4;
  int size3 = 10;
  int size4 = 2;

  int *new_tab = merge_array(tab, tab2, size, size2);
  size = size + size2;
  int i = 0;
  while (i < size)
    printf("%d ", new_tab[i++]);
  printf("\n");
  i = 0;
  int *tmp = new_tab;
  new_tab = merge_array(new_tab, tab3, size, size3);
  size = size + size3;
  free(tmp);
  while (i < size)
    printf("%d ", new_tab[i++]);
  printf("\n");
  int *new_tab2 = merge_array(tab3, tab4, size3, size4);
  size3 = size3 + size4;
  i = 0;
  while (i < size3)
    printf("%d ", new_tab2[i++]);
  printf("\n");
  tmp = new_tab;
  new_tab = merge_array(new_tab, new_tab2, size, size3);
  size = size + size3;
  free(tmp);
  i = 0;
  while (i < size)
    printf("%d ", new_tab[i++]);
  printf("\n");
  free(new_tab);
  return (0);
}
