#include <stdio.h>

int	magic_square(int *sqr)
{
  int	i = 0;
  int	val = sqr[0] + sqr[1] + sqr[2];
  int	start = 3;
  int	decal = 1;
  int	incr = 3;
  while (i < 7)
    {
      int	cmp = 0;
      int	j = 0;

      while (j < 3)
	{
	  cmp = cmp + sqr[start + decal * j];
	  ++j;
	}
      if (cmp != val)
	return (1);
      start = start + incr;
      if (i == 1)
	{
	  decal = 3;
	  start = 0;
	  incr = 1;
	}
      else if (i == 4)
	{
	  start = 0;
	  decal = 4;
	}
      else if (i == 5)
	{
	  start = 2;
	  decal = 2;
	}
      ++i;
    }
  return (0);
}

int	main()
{
  int tab[] = {0, 0, 0, 0, 0, 0, 0, 0, 0}; // True => ret 0
  int tab2[] = {0, 1, 2, 0, 1, 2, 0, 1, 2}; // False => ret 1
  int tab3[] = {8, 1, 6, 3, 5, 7, 4, 9, 2}; // True => ret 0

  printf("%d %d %d\n", magic_square(tab), magic_square(tab2), magic_square(tab3));
  return 0;
}
