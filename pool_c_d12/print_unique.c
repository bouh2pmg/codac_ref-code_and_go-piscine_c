#include <unistd.h>
#include <stdio.h>

int   if_valide(int *tab, int index, int size)
{
  int i = -1;
  while (++i < size)
  {
    if (tab[i] == tab[index] && index != i)
      return (0);
  }
  return (1);
}

void print_unique(int *tab, int size)
{
  int i = -1;
  char first = 0;

  while (++i < size)
  {
      if (if_valide(tab, i, size))
        printf((first++ == 0 ? "%d" : ",%d"), tab[i]);
  }
  printf("\n");
}

int	main()
{
  int tab[] = {0};
  int tab2[] = {0, 1, 2, 0};
  int tab3[] = {0, 1, 2, 3, 0, 74, 0, 0, 2, 2};
  int tab4[] = {2, 3};

  print_unique(tab, 1); // 0
  print_unique(tab2, 4); // 1,2
  print_unique(tab3, 10); // 1,3,74
  print_unique(tab4, 2); // 2,3
  return 0;
}
