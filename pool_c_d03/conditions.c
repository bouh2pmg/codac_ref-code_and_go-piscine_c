#include <unistd.h>

void my_putchar(char c)
{
    write(1, &c, 1);
}

void    conditions(int nb)
{
  write(1, ((nb > 0) ? "+" : (nb == 0 ? "0" : "-")), 1);
}

int   main()
{
  conditions(564);
  conditions(-12);
  conditions(0);
  return (0);
}
