#include <unistd.h>

void    my_true_loop(unsigned int nb)
{
  int i;

  i = -1;
  while (++i < nb)
    write(1, "+", 1);
  write(1, "\n", 1);
}

int   main()
{
  my_true_loop(5);
  return (0);
}
