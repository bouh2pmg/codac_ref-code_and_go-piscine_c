#include <unistd.h>

void my_putchar(char c)
{
write(1,&c,1);
}

void alpha()
{
  int i;

  i = 65;
  while (i <= 90)
    {
      my_putchar(i);
      i = i + 1;
    }
  my_putchar('\n');
  return ;
}

int main()
{
    alpha();
    return (0);
}
