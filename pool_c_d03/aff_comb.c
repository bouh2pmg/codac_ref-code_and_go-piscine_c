#include <unistd.h>

void my_putchar(char c)
{
  write(1, &c, 1);
}

int	show_number(int f, int s, int t)
{
  my_putchar(f);
  my_putchar(s);
  my_putchar(t);
  if (f != '7' || s != '8' || t != '9')
    {
      my_putchar(',');
      my_putchar(' ');
    }
  return (0);
}

int	my_aff_comb()
{
  char  i;
  char  j;
  char  k;

  i = '0';
  while (i <= '9')
    {
      j = i + 1;
      while (j <= '9')
	     {
	        k = j + 1;
	         while (k <= '9')
	          {
	             show_number(i, j, k);
	             k++;
	          }
	       j++;
	      }
      i++;
    }
  return (0);
}

int	main()
{
  my_aff_comb();
  return (0);
}
