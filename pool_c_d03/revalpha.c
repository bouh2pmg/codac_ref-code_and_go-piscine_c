#include <unistd.h>

void my_putchar(char c)
{
    write(1, &c, 1);
}

void revalpha()
{
  int i;

  i = 90;
  while (i >= 65)
    {
      my_putchar(i);
      i = i - 1;
    }
  my_putchar('\n');
  return ;
}

int main()
{
    revalpha();
    return (0);
}
